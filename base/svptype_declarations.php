<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter le champ `identifiant` à la table des mots.
 * Ce champ est une chaine sans espace qui représente un id textuel unique (l'unicité se définit
 * au sein d'un groupe de mots).
 *
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $tables Description des tables de la base.
 *
 * @return array Description des tables de la base complétée par celles du plugin.
 */
function svptype_declarer_tables_objets_sql(array $tables) : array {
	// Colonne 'identifiant'
	$tables['spip_mots']['field']['identifiant'] = "varchar(255) DEFAULT '' NOT NULL";

	return $tables;
}

/**
 * Déclarations des tables auxilliaires nécessaires au plugin :
 * - plugins_typologies, liens entre plugins et types de plugin.
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables Liste des tables auxillaires
 *
 * @return array Liste des tables auxillaires mises à jour.
 */
function svptype_declarer_tables_auxiliaires(array $tables) : array {
	// Tables de liens entre plugins et les types de plugins : spip_plugins_typologies
	$plugins_typologies = [
		'id_groupe' => 'bigint(21) DEFAULT 0 NOT NULL',
		'id_mot'    => 'bigint(21) DEFAULT 0 NOT NULL',
		'prefixe'   => "varchar(48) DEFAULT '' NOT NULL"
	];

	$plugins_typologies_key = [
		'PRIMARY KEY'   => 'id_mot, prefixe',
		'KEY id_groupe' => 'id_groupe'
	];

	$tables['spip_plugins_typologies'] = [
		'field' => &$plugins_typologies,
		'key'   => &$plugins_typologies_key
	];

	return $tables;
}

/**
 * Déclaration des informations tierces (alias, traitements, jointures, etc)
 * sur les tables de la base de données modifiées ou ajoutées par le plugin.
 *
 * Le plugin se contente de déclarer les alias des tables et des jointures.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface Tableau global des informations tierces sur les tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles informations
 */
function svptype_declarer_tables_interfaces(array $interface) : array {
	// Les tables
	$interface['table_des_tables']['plugins_typologies'] = 'plugins_typologies';

	// Les jointures
	// -- Entre spip_plugins_typologies et spip_plugins
	$interface['tables_jointures']['spip_plugins']['prefixe'] = 'plugins_typologies';
	$interface['tables_jointures']['spip_mots']['id_mot'] = 'plugins_typologies';

	return $interface;
}
