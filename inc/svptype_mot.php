<?php
/**
 * Ce fichier contient l'API de gestion des mots-clés propre à SVP Typologie.
 *
 * @package SPIP\SVPTYPE\MOT\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifie que le mot identifié par son id est un mot racine d'une typologie arborescente.
 *
 * @api
 *
 * @param int $id_mot Id du mot-clé.
 *
 * @return bool True si le mot est une racine d'une typologie arborescente, false sinon.
 */
function mot_est_racine(int $id_mot) : bool {
	// Initialisation du tableau statique des indicateurs de groupe typologique.
	static $est_racine = [];

	if (!isset($est_racine[$id_mot])) {
		$est_racine[$id_mot] = false;

		// Groupe et parent du mot
		if ($id_mot) {
			include_spip('action/editer_objet');
			$mot = objet_lire('mot', $id_mot, ['id_groupe', 'id_parent']);

			include_spip('inc/config');
			if ($configurations_typologie = lire_config('svptype/typologies', [])) {
				$est_arborescente = array_column($configurations_typologie, 'est_arborescente', 'id_groupe');
				if (!empty($est_arborescente[$mot['id_groupe']])) {
					$est_racine[$id_mot] = $mot['id_parent'] == 0;
				}
			}
		}
	}

	return $est_racine[$id_mot];
}

/**
 * Vérifie que le groupe identifié par son id matérialise bien une typologie de plugin.
 *
 * @api
 *
 * @param int $id_groupe Id du groupe de mots concerné.
 *
 * @return bool True si le groupe est celui d'une typologie, false sinon.
 */
function groupe_est_typologie_plugin(int $id_groupe) : bool {
	// Initialisation du tableau statique des indicateurs de groupe typologique.
	static $est_typologie = [];

	if (!isset($est_typologie[$id_groupe])) {
		$est_typologie[$id_groupe] = false;

		include_spip('inc/config');
		if ($configurations_typologie = lire_config('svptype/typologies', [])) {
			$ids_groupe = array_column($configurations_typologie, 'id_groupe');
			if (in_array($id_groupe, $ids_groupe)) {
				$est_typologie[$id_groupe] = true;
			}
		}
	}

	return $est_typologie[$id_groupe];
}

/**
 * Renvoie l'id du groupe d'un mot-clé.
 *
 * @api
 *
 * @param int $id_mot Id du mot-clé.
 *
 * @return int Id du groupe d'appartenance du mot-clé ou 0 sinon.
 */
function mot_lire_groupe(int $id_mot) : int {
	static $ids_groupe = [];

	if (!isset($ids_groupe[$id_mot])) {
		$ids_groupe[$id_mot] = 0;

		$from = 'spip_mots';
		$where = ['id_mot=' . (int) $id_mot];
		$id = sql_getfetsel('id_groupe', $from, $where);
		if ($id !== null) {
			$ids_groupe[$id_mot] = (int) $id;
		}
	}

	return $ids_groupe[$id_mot];
}

/**
 * Renvoie le champ `identifiant` d'un mot-clé si celui-ci est un type de plugin.
 * Si le mot-clé n'est pas un type de plugin l'identifiant est vide.
 *
 * @api
 *
 * @param int $id_mot Id du mot-clé.
 *
 * @return string Champ `identifiant` du mot-clé si celui-ci est un type de plugin ou chaine vide sinon.
 */
function mot_lire_identifiant(int $id_mot) : string {
	static $identifiants = [];

	if (!isset($identifiants[$id_mot])) {
		$identifiants[$id_mot] = '';

		$from = 'spip_mots';
		$where = ['id_mot=' . (int) $id_mot];
		$identifiant = sql_getfetsel('identifiant', $from, $where);
		if ($identifiant !== null) {
			$identifiants[$id_mot] = $identifiant;
		}
	}

	return $identifiants[$id_mot];
}

/**
 * Renvoie les id des enfants d'un mot-clé.
 *
 * @param int $id_mot Id du mot-clé.
 *
 * @return array Tableau des id des enfants d'un mot-clé ou tableau vide sinon.
 */
function mot_lire_enfants(int $id_mot) : array {
	static $ids_enfant = [];

	if (!isset($ids_enfant[$id_mot])) {
		$ids_enfant[$id_mot] = [];

		$from = 'spip_mots';
		$where = ['id_parent=' . (int) $id_mot];
		$ids = sql_allfetsel('id_mot', $from, $where);
		if ($ids) {
			$ids_enfant[$id_mot] = array_column($ids, 'id_mot');
		}
	}

	return $ids_enfant[$id_mot];
}
