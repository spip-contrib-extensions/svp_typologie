<?php
/**
 * Action pour désaffecter un type de plugin d'un plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour désaffecter un type de plugin d'un plugin.
 *
 * Cette action est réservée aux utilisateurs pouvant affecter un type de plugin.
 * L'argument attendu est `prefixe:id_mot`.
 *
 * @uses plugin_desaffecter_type_plugin()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_desaffecter_plugin_dist(?string $arguments = null) : void {
	// Récupération des arguments de façon sécurisée.
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}

	$arguments = explode(':', $arguments);
	[$id_plugin, $prefixe, $id_mot] = $arguments;

	// Verification des autorisations
	include_spip('inc/autoriser');
	if (!autoriser('affecter', 'type_plugin', $id_plugin)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	include_spip('inc/svptype_plugin');
	plugin_desaffecter_type_plugin($prefixe, $id_mot);
}
